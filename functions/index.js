const functions = require('firebase-functions');
const admin = require('firebase-admin');
const express = require('express');
const bodyParser = require('body-parser');
const passport = require('./config/passport');


admin.initializeApp(functions.config().firebase);
/*
var serviceAccount = require("../wetherbee-45575-firebase-adminsdk-ujlhw-51a4ea8a6b.json");
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://wetherbee-45575.firebaseio.com"
});
*/
const db = admin.firestore();
const firestore = admin.firestore;
const app = express();

require('./routes')(app, db, firestore);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(passport.initialize(db));

exports.api = functions.https.onRequest(app);
