const passport = require("passport");
const Strategy = require("passport-jwt").Strategy;
const ExtractJwt = require("passport-jwt").ExtractJwt;

// import User from "../models/user.model";

exports.initialize = (db) => {
  var opts = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderWithScheme("jwt"),
    secretOrKey: 'my secret',
  };
  const userCollection = db.collection('AppUser');
  passport.use(
    new Strategy(opts, function (jwt_payload, done) {
      userCollection.doc(jwt_payload.phone).get().then(snapshot => {
        if (snapshot.empty) {
          return done(null, false);
        }
        const user = { id: snapshot.id, ...snapshot.data() };
        return done(null, user);
      });
    })
  );
  return passport.initialize();
}

exports.authenticate = (req, res, next) => {
  passport.authenticate("jwt", { session: false }, (err, user, info) => {
    if (err) {
      return next(err);
    }
    if (!user) {
      res.status(401).json({
        error: {
          message: info.message
        }
      })
    }
    req.user = user;
    return next();
  })(req, res, next);
}
