module.exports = (router, db, firestore) => {
  require('./auth')(router, db, firestore);
  require('./question')(router, db, firestore);
  require('./analytics')(router, db, firestore);
};
