const baseUrl = '/question';
const passport = require('../config/passport');
const moment = require("moment");

module.exports = (router, db, firestore) => {

  router.get(`${baseUrl}/today-question`, passport.authenticate, async (req, res) => {
    // currently it loads last question
    const questionSetCollection = db.collection('Questions');
    const questionSet = await questionSetCollection.get().then(snapshot => {
      return snapshot.docs[0];
    })
    const questionCollection = questionSetCollection.doc(questionSet.id).collection('question')
    let question = await questionCollection.get().then(snapshot => {
      return snapshot.docs[0];
    })
    question = { ...question.data(), id: question.id, questionSetId: questionSet.id };

    return res.json({
      question
    })
  })

  router.post(`${baseUrl}/answer`, passport.authenticate, async (req, res) => {
    const userId = req.user.id;
    let { question, answer } = req.body;

    // update question correct/incorrect data
    const questionSetDB = db.collection('Questions');
    const questionRef = questionSetDB.doc(question.questionSetId).collection('question').doc(question.id);
    // re-define var question
    if (question.answer == answer) {
      questionRef.set({ noOfCorrect: question.noOfCorrect+1 }, {merge: true});
    } else {
      let { UsersGotWrong } = question;
      UsersGotWrong.push(userId);
      questionRef.set({ noOfWrongs: question.noOfWrongs+1, UsersGotWrong }, {merge: true});
    }

    // update QAnwsers collection
    const qAnswersDB = db.collection('QAnswers');
    await qAnswersDB.add({
      questionId: question.id,
      questionSetId: question.questionSetId,
      userId,
      date: firestore.Timestamp.now(),
      answer
    })

    return res.json({
      status: 'success'
    })
  })
}
