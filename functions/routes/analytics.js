const baseUrl = '/analytics';
const passport = require('../config/passport');
const moment = require('moment');

module.exports = (router, db, firestore) => {

  router.get(`${baseUrl}/for-user`, passport.authenticate, async (req, res) => {
    const userId = req.user.id;
    const qAnswersDB = db.collection('QAnswers');
    const questionDB = db.collection('Questions');
    const { queryCondition } = req.query;

    const today = moment().toDate();
    let startDate = moment(today).startOf('year').toDate();
    let endDate = moment(today).endOf('year').toDate();
    if (queryCondition=='weekly') {
      startDate = moment(today).startOf('week').toDate();
      endDate = moment(today).endOf('week').toDate();
    } else if (queryCondition=='monthly') {
      startDate = moment(today).startOf('month').toDate();
      endDate = moment(today).endOf('month').toDate();
    }
    let qAnswers = [], qAnswersOfEnglish = [], qAnswersOfMath = [];
    await qAnswersDB
      .where('userId', '==', userId)
      /*
      .where('date', '>=', startDate)
      .where('date', '<=', endDate)
      */
      .get().then(async snapshot => {
        for (let qAnswerRef of snapshot.docs) {
          let qAnswer = qAnswerRef.data();
          let question = await questionDB.doc(qAnswer.questionSetId).collection('question')
            .doc(qAnswer.questionId).get().then(snapshot => {
              return snapshot.data();
            });
          qAnswer = {...qAnswer, question};
          qAnswers.push(qAnswer);
          if (question.questionCategory=='English') {
            qAnswersOfEnglish.push(qAnswer);
          } else if (question.questionCategory=='Math') {
            qAnswersOfMath.push(qAnswer);
          }
        }
        return;
      });

    let stepDates = [];
    let chartData = [];
    // calc dates per step
    if (queryCondition=='weekly') {
      let stepStartDate = startDate;
      let stepEndDate = moment(stepStartDate).endOf('day').toDate();
      let labels = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
      let labelIndex = 0;
      while (moment(stepEndDate).isBefore(moment(endDate).add('days', 1))) {
        let isActive = false;
        if (moment(today).isBefore(stepEndDate) && moment(today).isAfter(stepStartDate)) isActive = true;
        chartData.push({ stepStartDate, stepEndDate, label: labels[labelIndex], isActive });
        stepStartDate = moment(stepStartDate).add('days', 1).toDate();
        stepEndDate = moment(stepStartDate).endOf('days').toDate();
        labelIndex++;
      }
    } else if (queryCondition=='monthly') {
      let stepStartDate = startDate;
      let stepEndDate = moment(stepStartDate).endOf('week').toDate();
      let labels = ['1st Week', '2nd Week', '3rd Week', '4th Week', '5th Week', '6th Week'];
      let labelIndex = 0;
      do {
        let label = moment(stepStartDate).format('D')+'~'+moment(stepEndDate).format('D');
        let isActive = false;
        if (moment(today).isBefore(stepEndDate) && moment(today).isAfter(stepStartDate)) isActive = true;
        chartData.push({ stepStartDate, stepEndDate, label, isActive });
        stepStartDate = moment(stepStartDate).startOf('week').add('weeks', 1).toDate();
        stepEndDate = moment(stepStartDate).endOf('weeks').toDate();
        if (moment(stepEndDate).isAfter(endDate)) {
          if (moment(stepStartDate).isAfter(endDate)) break;
          stepEndDate = endDate;
          label = moment(stepStartDate).format('D')+'~'+moment(stepEndDate).format('D');
          let isActive = false;
          if (moment(today).isBefore(stepEndDate) && moment(today).isAfter(stepStartDate)) isActive = true;
          chartData.push({ stepStartDate, stepEndDate, label, isActive });
          break;
        }
        labelIndex++;
      } while (moment(stepEndDate).isBefore(moment(endDate).add('weeks', 1)))
    } else {
      let stepStartDate = startDate;
      let stepEndDate = moment(stepStartDate).endOf('month').toDate();
      let labels = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
      let labelIndex = 0;
      while (moment(stepEndDate).isBefore(moment(endDate).add('months', 1))) {
        let isActive = false;
        if (moment(today).isBefore(stepEndDate) && moment(today).isAfter(stepStartDate)) isActive = true;
        chartData.push({ stepStartDate, stepEndDate, label: labels[labelIndex], isActive });
        stepStartDate = moment(stepStartDate).add('months', 1).toDate();
        stepEndDate = moment(stepStartDate).endOf('months').toDate();
        labelIndex++;
      }
    }

    // loop and calculate ranking & scores within date range
    for (let chartDateItem of chartData) {
      let scoresOfAllUser = {}, scoreOfMy = 0;
      await qAnswersDB
        .where('date', '>=', chartDateItem.stepStartDate)
        .where('date', '<=', chartDateItem.stepEndDate)
        .get().then(async snapshot => {
          for (let qAnswerRef of snapshot.docs) {
            let qAnswer = qAnswerRef.data();
            let question = await questionDB.doc(qAnswer.questionSetId).collection('question')
              .doc(qAnswer.questionId).get().then(snapshot => {
                return snapshot.data();
              });
            scoresOfAllUser[qAnswer.userId] =  scoresOfAllUser[qAnswer.userId] || 0;
            if (question.answer==qAnswer.answer) {
              if (question.questionDifficulty=='Hard') {
                scoresOfAllUser[qAnswer.userId] += 3;
                if (qAnswer.userId==userId)
                  scoreOfMy += 3;
              } else if (question.questionDifficulty=='Medium') {
                scoresOfAllUser[qAnswer.userId] += 2;
                if (qAnswer.userId==userId)
                  scoreOfMy += 2;
              } else {
                scoresOfAllUser[qAnswer.userId] += 1;
                if (qAnswer.userId==userId)
                  scoreOfMy += 1;
              }
            }
          }
        })
      chartDateItem['scoresOfAllUser'] = scoresOfAllUser;
      chartDateItem['scoreOfMy'] = scoreOfMy;
      let ranking = 1;
      for (let [individualUserId, scoreEach] of Object.entries(scoresOfAllUser)) {
        if (scoreEach>scoreOfMy) ranking++;
      }
      chartDateItem['ranking'] = ranking;
    }

    return res.json({
      chartData, qAnswers, qAnswersOfEnglish, qAnswersOfMath
    })
  })
}
