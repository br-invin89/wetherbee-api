const bcrypt = require("bcrypt");
const baseUrl = "/auth";
const jwt = require("jsonwebtoken");
const moment = require("moment");

module.exports = (router, db, firestore) => {
  router.post(`${baseUrl}/login`, async (req, res) => {
    let { phone, password } = req.body;

    const userCollection = db.collection("AppUser");
    let user = await userCollection.doc(phone).get();
    if (user.exists) {
      user = user.data();
      bcrypt.compare(password, user.password, (err, isMatch) => {
        if (err) {
          return res.json({
            error: {
              message: "Error on database",
            },
          });
        }
        if (isMatch) {
          var token = jwt.sign(user, "my secret", {
            expiresIn: "3d",
          });
          // return the information including token as JSON
          const createdAt = moment(user.createdAt.toDate()).format("YYYY-MM-DD");
          const updatedAt = moment(user.updatedAt.toDate()).format("YYYY-MM-DD");
          user = { ...user, createdAt, updatedAt };
          res.json({ token: "JWT " + token, user });
        } else {
          res.json({
            error: {
              message: "Password is incorrect.",
            },
          });
        }
      });
    } else {
      return res.json({
        error: {
          message: "Phonenumber is incorrect.",
        },
      });
    }
  });

  router.post(`${baseUrl}/signup`, async (req, res) => {
    let {
      name,
      email,
      password,
      phone,
      schoolName,
      grade,
      examTypes,
    } = req.body;
    password = await bcrypt.hash(password, 10);

    const userCollection = db.collection("AppUser");
    const isExist = await userCollection
      .doc(phone)
      .get()
      .then((snapshot) => {
        if (snapshot.exists) {
          return true;
        }
      });
    if (isExist) {
      return res.json({
        error: {
          message: "Your phonenumber is already taken.",
        },
      });
    }
    await userCollection.doc(phone).set({
      phone,
      password,
      personalInfo: {
        name,
        email,
        phone,
        schoolName,
        grade,
      },
      examTypes,
      createdAt: firestore.Timestamp.now(),
      updatedAt: firestore.Timestamp.now(),
    });

    return res.json({
      message: "User created successfully.",
    });
  });
};
